#' Create a report of z_scores for miRNA seeds
#'
#' @param genes a vector of gene names
#' @param lfc = a vector of log2FoldChange or other variable to be used to calculate the z-score
#' @param seeds a list of seeds to be used. Default = "all"
#' @param conserved_targets_only Use only targets with conserved sites? Default = T
#' @param organism Default = "mouse" options ("mouse", "human")
#'
#' @return a tibble containig seeds, corresponding miRNA, z scores, pvalues, fdr, and gene set size
#' @export
#'
#' @examples
#' report <- generate_z_report(genes = mir_1792_ko_df$gene_symbol, lfc = mir_1792_ko_df$ko.log2FoldChange, organism = "mouse", seeds = "all", conserved_targets_only = T)
#' head(report)

generate_z_report <- function(genes, lfc, seeds = "all", conserved_targets_only = T, organism = "mouse"){
  #initial setup
  if(organism == "mouse"){
    mirmap = mir_map
    if (conserved_targets_only){
      targetscan <- mm_targetscan_conserved
    } else {
      targetscan <- mm_targetscan
    }
  }
  if(organism == "human"){
    mirmap = hs_mir_map
    targetscan <- hs_targetscan_conserved
        # } else {
        #   targetscan <- hs_targetscan_conserved
        # }
  }
  if (seeds == "all"){
    seeds = unique(targetscan$miRNA.family)
  } else {
    seeds = intersect(seeds, unique(targetscan$miRNA.family)) #uses only seeds actually represented in targetscan
  }

 # calculate z score for each seed
  z_score = numeric()
  z_score_random = numeric()
  gene_set_size = numeric()
  i = 1
  miRNA = character()
  repeat{
    seed = seeds[i]
    miRNA = c(miRNA, mirmap[mirmap$miRNA.family == seed,]$miRNA)
    gene_set = unique(targetscan[targetscan$miRNA.family == seed, "Gene.Symbol"])
    z = gene_set_z_score(genes = genes, logFC = lfc, geneset = gene_set)
    z_score = c(z_score, z[1])
    gene_set_size = c(gene_set_size, z[2])
    i = i+1
    if (i>length(seeds)){
      break
    }
  }
  pvalues = pnorm(-abs(z_score))
  report <- tibble(seed = seeds, miRNA = miRNA, z_score = z_score, pvalue = pvalues, fdr = p.adjust(pvalues), gene_set_size = gene_set_size)
  return (report)

}

