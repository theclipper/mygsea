#' generates a Gene Set Enrichment Plot from either a gene set or a vector of miRNA seeds.
#'
#' @param genes  a character vector with the gene symbols
#' @param lfc  a character vector with the log2fold change values in the same order as the gene symbols
#' @param seeds a list of miRNA seeds to be used to generate the gene_set. Defalt = "". Either "seeds" or "gene_set" need to be provided. If both are provided, "seeds" is used.
#' @param organism organism default = "mouse". Options: "human", "mouse".
#' @param conserved_targets_only Use only conserved miRNA binding sites? Default = True
#' @param color plot color (defaul+ "lightblue")
#' @param preranked Is the gene list ranked? Default = F
#' @param title Title of the plot
#' @param calculate_nES Should calculate nES for each gene set? Default = False. If set to True computation will require longer.
#'
#' @param permutations number of permutations to be used if calculate nES is true. Default = 500
#'
#' @return a ggplot2 object
#' @export
#'
#' @examples
enrichment_plot <- function(genes, lfc, seeds = "",
                          gene_set = "",
                          organism = "mouse",
                          conserved_targets_only = T,
                          color = "lightblue",
                          preranked = F,
                          title = "GSEA_plot",
                          calculate_nES = F,
                          permutations = 500){
  if(gene_set[1] == "" & seeds[1] == ""){
    print ("please provide a geneset or a list of seeds")
    return()
  }
  if(seeds[1]!=""){
    gene_set = extract_gene_set(seeds = seeds, conserved_targets_only = conserved_targets_only, organism = organism)
  }
  gene_list = as.data.frame(tibble(genes = genes, lfc = lfc))
  ES <- calculate_ES(gene_list = gene_list, gene_set = gene_set, preranked = preranked, calculate_NES = calculate_nES, permutations = 500)
  plot_ES(ES, color = color, title = title)
}


