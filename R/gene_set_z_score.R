require("tidyverse")
#' calculates z_score for a gene set according Kim and Volsky (BMC Bioinformatics 144, 2005) much faster than ES and nES!
#'
#' @param gene_list a character vector of gene symbols
#' @param logFC a vector containing the logFC for the genes
#' @param geneset a character vector of gene symbols corresponding to the geneset
#'
#' @return a numeric vector containing a z score and the effective size of the gene set
#' @export
#'


gene_set_z_score <- function(genes, logFC, geneset){
  df <- tibble(genes = genes, logFC = logFC)
  geneset = intersect(genes, geneset)
  n = length(geneset)
  N = length(genes)
  lfc <- df[df$genes %in% geneset, "logFC"]
  m <- mean(lfc$logFC) #mean logFC of the the entire dataset
  M <- mean(logFC) #mean logFC of the gene set
  stdev <- sd(logFC)
  z <-  ((m-M)*sqrt(n))/stdev
  return (c(z, n))
}
